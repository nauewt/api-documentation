.. _nau-map

=============================
NAU Interactive Map Reference
=============================

GET Parameters Understood by the Front-End Web App
==================================================

* **buildingNum** -- The building number for the building to show on page load
    * **possible values** (any building number, such as 28 for Cline Library)
    * **default value** (none)
* **tags** -- Comma-separated list of categories specifying which locations will be pinned on load
    * **possible values** building, residential, tour, emergency, bluelight
    * **default value** (none)
* **sidebar** -- Determines if the sidebar should be visible
    * **possible values** 1, 0
    * **default value** 1
* **topbar** -- Determines if the topbar should be visible
    * **possible values** 1, 0
    * **default value** 1
* **intro** -- Determines if the intro overlay should be shown on load
    * **possible values** 1, 0
    * **default value** 0

.. The information as a table, which doesn't display properly after being deployed to api.nau.edu:
    =========   =============================   =============   =========================================================================================
    Parameter   Possible Values                 Default Value   Description
    =========   =============================   =============   =========================================================================================
    tags        building, residential, tour, 
                emergency, bluelight            (none)          Comma-separated list of categories specifying which locations will be pinned on page load
    sidebar     1, 0                            1               Determines if the sidebar should be visible
    topbar      1, 0                            1               Determines if the topbar should be visible
    =========   =============================   =============   =========================================================================================

Examples
--------

* Show a tour of campus:
    http://www2.nau.edu/nau-map/?tags=tour&sidebar=0&topbar=0
* Pin residence halls on page load:
    http://www2.nau.edu/nau-map/beta/?tags=building,residential

.. TODO: How to Interact with the GeoDB Web Service
