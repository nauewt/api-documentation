.. _psdata

.. TODO: This documentation needs to be fleshed out a little

=============================================
NAU PeopleSoft Data Resource Server Reference
=============================================

What is the PeopleSoft Data Resource Server?
============================================

**TODO**


OAuth2 EndPoints
================

All OAuth2 endpoints use the currently authorized user, and are located at /psdata/me.


GET /psdata/me/addresses
------------------------

Get addresses.

* Parameters

  * **addrType**: (optional) Get a specific address: BILL, BUSN, CBOX, CHK, CRES, DIPL, HOME, MAIL, OTH2, PERM, RFND, W9, WKBX, WORK


GET /psdata/me/emails
---------------------

Get emails.

* Parameters

  * **emailType**: (optional) Get a specific email: PERS, STDT, WORK NAU


GET /psdata/me/names
--------------------

Get names.

* Parameters

  * **nameType**: (optional) Get a specific name: DEG, FR[1-6], FTR, LEG, MDN, MTR, OTH, PRF, PRI


GET /psdata/me/phones
---------------------

Get phone numbers.

* Parameters

  * **phoneType**: (optional) Get a specific number: BUSN, CAMP, CELL, DAY, DORM, EVE, FAX, HOME, LOCL, MAIL, MAIN, OTH, PERM,PGR1, PGR2,TELX, W-9, WINT, WKC2, WKCT, WORK


GET /psdata/me/gpa
------------------

Get GPA.

* Parameters

  * **termType**: (optional) Get GPA for a specific term (eg: 1127)


GET /psdata/me/gender
---------------------

Get gender.

* Parameters

  * *none*


GET /psdata/me/gradstatus
-------------------------

Get graduation status.

* Parameters

  * *none*
