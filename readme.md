# Overview

This project provides publicly available documentation for the API of various other projects. It is available at these locations:

* [dev](https://api-dev.nau.edu)
* [prd](https://api.nau.edu)

sphinx_rtd_theme is recommended, to enable viewing files with a style similar to how they will appear when uploaded to api.nau.edu:

* [rtd theme](https://github.com/snide/sphinx_rtd_theme)

# How to Add Documentation for a New Project

1. Create a new folder in the root directory that is named after your project
2. Create an index.rst file in that folder that starts with `.. _project_name`
3. Write Markdown-formatted content in that file. Comments look like this: `.. TODO: Write more in this section`.
4. Edit index.rst in the root directory to add a new section pointing to the index.rst file for your project

# How to Build

The Sphinx Python Document Generator is required to build this project. It is available here:
[Sphinx](http://sphinx-doc.org)

Use the command `make html`, then look in `_build/html/` for the result.
