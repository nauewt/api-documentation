.. NAU API documentation master file, created by
   sphinx-quickstart on Tue Jan 21 11:20:13 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
Welcome to NAU API's documentation!
===================================

OAuth2 Client / Server Documentation
====================================

.. toctree::
   :maxdepth: 2

   oauth2/index
   oauth2/pylons
   oauth2/ticket


Documentation by Resource Server
================================

.. toctree::
   :maxdepth: 2

   psdata/index

.. toctree::
    :maxdepth: 2

    nau-map/index
