.. _oauth2

====================
NAU OAuth2 Reference
====================

OAuth2 Authentication
=====================

If your applications needs access to a user's private data, the user can authorize your application to have access to that data for a time using OAuth2. The user is allowed to decide what information you are allowed to access. For example, any student or staff data in PeopleSoft.

Before being able to perform OAuth2 requests, your application must be registered with the service. This uniquely identifies your application and what data you can request.

Process
-------

The authentication process has multiple steps:

* Stage 1 - Authentication / Auth Code

  * Your application receives a new request from a user.
  * You redirect the user's browser to the OAuth2 authorization URI, and provide a callback URI to your application. *(see /oauth2/auth)*
  * The user authenticates with CAS and is then given a list of scopes that your application will have access to, which they can approve or deny. A scope represents a discrete set of data (eg, 'bio_address' might give the app access to the user's home address).
  * If they approve, they will be redirected back to your application callback URI. An authorization code will be provided to your app in the 'code' GET parameter.

* Stage 2 - Get Access Token

  * Using the authorization code from the previous step, your application will then make an HTTP POST request to the OAuth2 server providing this code and your client information. *(see /oauth2/token)*
  * The OAuth2 server will provide, in JSON, an access token, a refresh token, and an expiration time. Your application should save this information in the user's session.
  * When the token expires, your application can request a new one during the same session using the refresh token.

* Stage 3 - Request Protected Data

  * Once your application has an access token, you can make requests directly to the desired web service using their API.
  * Your application will provide the access token to the resource server in the HTTP 'Authentication' header as a bearer token.
  * The resource server will independently verify your authorization with the OAuth2 server using the token you provided.
  * Repeat this step until the access token expires. Your app may then request another from the OAuth2 server using the refresh token.

There are several existing OAuth2 client libraries for many different lanugages. We provide a handful of client libraries / samples.


Endpoints
=========

All parameters are required and should be string values unless otherwise specified.

GET /oauth2/auth
----------------

Redirect the user's browser to this page with the required parameters to begin the authorization process.

* Parameters

  * **client_id**: Your app's unique registered identifier.
  * **redirect_uri**: The URI of your app where the user will be redirected.
  * **response_type**: Always the value 'code'.
  * **scope**: A space delimited list of scopes being requested.
  * **state**: Currently unused.

* Returns (after authentication) JSON response body containing

  * **code**: Authorization code used to request an access token.


POST /oauth2/token
------------------

Takes an authentication code (from /auth) and returns an access token needed to actually make requests for the protected resource.

* Parameters

  * **client_id**: Your app's unique registered identifier.
  * **client_secret**: Your app's shared secret.
  * **grant_type**: Either "authorization_code" or "refresh_token". See below.
  * **code**: Required if grant type is "authorization_code"; the authorization code recieved from /oauth2/auth *if* that code has not yet been redeemed.
  * **refresh_token**: Required if grant type is "refresh_token"; the refresh token recieved from a previous call to /oauth2/token. Can only be used when the previously issued access token has expired.

* Returns either an HTTP error status or 200 and a JSON response body containing

  * **access_token**: The access token used to request data.
  * **refresh_token**: The refresh token used to get a new access token after expiration.
  * **expires**: [integer] Unix timestamp when this token will expire.
  * **expires_in**: [integer] Time until this token expires in seconds.


POST /oauth2/access
-------------------

* Parameters

  * **client_id**: Your app's unique registered identifier.
  * **client_secret**: Your app's shared secret.
  * **grant_type**: Either "authorization_code" if getting making an initial request with an auth code or "refresh_token" if refreshing.
  * **code**: Authorization code, required if grant_type is "authorization_code"
  * **refresh_token**: Required if grant_type is "refresh_token"

* Returns either an HTTP error status or 200 and a JSON response body containing

  * **authorized**: Always true if response is 200.
  * **user_id**: The id or uid of the user for which access is granted.
  * **service**: [optional] Will be present and true if the access is from a service application.
  * **pgtId**: [optional] Will be present if the application is allowed access to proxy granting tickets.
  * **pgtIou**: [optional] Will be present if the application is allowed access to proxy granting tickets.



Making Requests to Resource Servers
===================================

For specific API references for resource servers, see their `documentation <../index.html>`_. One thing that all NAU OAuth2 resource servers have in common is the method of presenting your authentication token for access.

All requests to resource servers must include an HTTP Authentication header, as follows::

  'Authentication': 'Bearer <your_access_token_here>'