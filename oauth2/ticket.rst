.. _oauth2/ticket

===========================
NAU OAuth2 Ticket Reference
===========================

OAuth2 Single Use Tickets
=========================

OAuth Tickets are a simple way for resource servers to authorize access to a resource exactly one time from anywhere that ticket is presented. The ticket cannot be used again, and must be redeemed within a small time window.

For example, your resource server may want to allow access to user images in the form of a standard URI that can be used with an <img> tag. The problem is that while an application can make requests to the resource server and authenticate on behalf of the user, the user's browser cannot. This can be solved with tickets.

You would request a ticket from OAuth, include the ticket in the image URI, and give that to the application. The user's browser will then request that image URI directly from your resource server. You simply redeem the ticket, and if valid, serve the image.

This is one possible use of tickets.

Process
-------

The ticket process has multiple steps:

* Stage 1 - Request Tickets / Authorization

  * The registered OAuth application must have the 'oauth/ticket' scope in order for your resource server to be able to request tickets.
  * The resource server makes a request to the OAuth2 server at the /ticket endpoint (see docs below). Depending on how you structure your request, you can request 1 (default) or multiple tickets.
  * In your request you need to provide an Authorization header containing a bearer token from the application.
  * Your request may contain arbitrary data attached to the ticket which will be returned when the ticket is redeemed.
  * The server will return an 'application/json' response containing an array of ticket objects. The ticket objects contain the ticket string itself as well as some meta information about the ticket expire time, etc.
  * You may distribute the tickets in any way you wish, but keep in mind that the ticket will allow anyone to make a request with that ticket until it's used or expires, so make sure the ticket is given only to people you want to have access.

* Stage 2 - Redeem Ticket

  * When your resource server receives a request containing a ticket, it will then make a POST request to the /ticket/redeem endpoint on the OAuth server with a 'ticket' POST param. No other authorization is required.
  * If the ticket is valid, the OAuth server will return an 'application/json' response containing the user_id and scope of the original bearer token and any extra data that was stored with the ticket.


Endpoints
=========

All parameters are required and should be string values unless otherwise specified.

GET|POST /oauth2/ticket
-----------------------

Create and return one or more tickets. If a POST request is made with a known data type and the body is parseable into an array (JSON, XML, or form encoded), the count parameter will be ignored and one ticket will be issued for each item in the array, with the array element being stored in the ticket's data field.

If the body is not an array, or is sent with a raw data type, the count parameter will be honored and every ticket generated will contain the raw body and its data.

Data is always optional.

* Parameters

  * **count**: The number of tickets to generate (optional, defaults to 1)

* Body

  * (Array|Mixed) Data to attach to ticket(s)

* Returns either an HTTP error status or 200 and (after authentication) a JSON response body containing an array of ticket objects with the following keys:

  * **ticket**: The unique string identifying the ticket.
  * **user_id**: The user id corresponding to the authorized bearer token.
  * **expires_at**: A timestamp representing when the ticket will expire if not used.
  * **data**: Arbitrary data associated with the ticket.

* HTTP Statuses

  * **200**: Ticket(s) generated successfully and returned in body.
  * **400**: Invalid request, bad or no authorization header provided.
  * **401**: Invalid authorization token.
  * **403**: Client not authorized by user or client does not have ticket privileges.


POST /oauth2/ticket/redeem
--------------------------

Redeems a single ticket if that ticket is valid and not expired. Once redeemed, a ticket can no longer be used.

* Parameters

  * **ticket**: The ticket identifier to redeem.

* Returns either an HTTP error status or 200 and a JSON response body containing

  * **user_id**: The user id corresponding to the authorized bearer token.
  * **scope**: The application scope corresponding to the authorized bearer token.
  * **data**: Arbitrary data associated with the ticket.

* HTTP Statuses

  * **200**: Ticket redeemed successfully.
  * **403**: Ticket has expired.
  * **404**: Ticket does not exist or was redeemed or expired and was cleaned from the database.


