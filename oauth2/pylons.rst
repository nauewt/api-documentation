.. _pylons

.. TODO: Lots of things need to be fleshed out or clarified in this document.

===========================
NAU OAuth2 Pylons Reference
===========================


What is Pylons?
===============

Pylons is a library for creating and using OAuth2 resource servers in Node.js Express applications. Included are objects for creating resource servers, standard OAuth2 clients, and OAuth2 service clients.


Resource Server
=================

Description
-----------

**TODO**


Example Usage
-------------

**TODO**


Instance Methods
----------------

set
^^^

sets the values for realm, tokenValidationUri, scopePrefix, scopes based on options passed in


setScopes
^^^^^^^^^

returns all the scopes defined in the production.json config file.
Scopes are set as follows::

    { '(/\\w+)*/person/me/addresses': [ 'edu.nau.peoplesoft/bio/addresses' ] ... }



getAuthenticationMiddleware
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Returns an Express middleware. Applied to all requests.
Compares the url path to the this.scopes list of paths and get the scope
Checks for the [Aa]uthorization header in the request and matches for bearer pattern "/Bearer\s+(.+)$/i"
If the pattern does not match sets fields 'WWW-Authenticate' => "Bearer realm='" + realm + "' scope='" + scopes + "'"
If pattern matches get the token is extracted from authorization header then a request is made to the oauth server '/oauth2/access' with the following headers::

    headers: {
      'Authorization': 'Bearer ' + token,
      'Scope': scopes
    }

Status codes between 400 and 500 are appropriately dealt with
req.oauthAccess is set to body from the request made above
If req.session is defined then req.session.oauthAccess is set to req.oauthAccess


Module Convenience Methods
--------------------------

authenticationMiddlware
^^^^^^^^^^^^^^^^^^^^^^^

returns an instance of the ResourceServer. (new ResourceServer()).getAuthenticationMiddleware(options)


attach
^^^^^^

Creates a new resource server object and "attaches" it to your existing express application (creates and applies authentication middleware).



Service Client
=================


Description
-----------

**TODO**


Example Usage
-------------

Using a service client is relatively simple compared to a standard OAuth2 client. You must still make a request to the OAuth2 server to get an access token, but may then make resource server requests as normal without having to get permission from the user. Typically, service accounts are only granted in special cases.

First, create the service account object::

    serviceClient = new ServiceClient({
        'client-id': client-id,
        'client-secret': client-secret,
        'client-key': client private key,
        'oauth2-uri': oauth server uri,
        'token-path': pstoken path '/oauth2/token',
        'authorization-path': authorization path '/oauth/auth']
    })

Then, authorize (refreshing access tokens is handled automatically)::

    serviceClient.authorize({ user_id: 'aaa111' }, function (err, auth) {
        if (auth) {
            // Use your access token
            req.session.auth = auth
        }
    })


Instance Methods
----------------

set
^^^

setter for client-id, client-secret, client-key, oauth2-uri, token-path, authorization-path, uid


validateOptions
^^^^^^^^^^^^^^^

Checks to see if all these var are set clientID, clientSecret, clientKey, site. tokenPath, authorizationPath and uid


shouldRefresh
^^^^^^^^^^^^^

returns true if this.auth.token.access_token AND this.auth.token.expires_at AND currentTime > this.auth.token.expires_at


encryptMessage
^^^^^^^^^^^^^^

returns json { message: encrypted Message (UTF8) base64, signature hash and sign (sha256) base 64}


getAuthorizationHeader
^^^^^^^^^^^^^^^^^^^^^^

returns authorization header. Ecrypts the options " Service clientID message signature "


getAuthCode
^^^^^^^^^^^

returns authcode. Makes get request to the oauth server '/oauth2/auth'


getAccessToken
^^^^^^^^^^^^^^

returns auth object. Makes a request to the oauth server '/oauth2/token'. These values are populated if they exist:

    access_token, refresh_token, expires_in, expires_at


authorize
^^^^^^^^^

authorizes a user.
Checks to see if shouldRefresh() method is true runs the getAccessToken(callback)
else getAccessToken(callback)